package com.everis.escuela.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.everis.escuela.models.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long>{

}
